<?php


class AppException extends Exception
{
    function __toString()
    {
    return "Your code has just thrown an exception: {$this->message}\n";
    }
}

class Students
{
    public $first_name;
    public $last_name;

    public function __construct($first_name, $last_name)
    {
    if(empty($first_name))
    {
        throw new AppException('First Name is required', 1);
    }

    if(empty($last_name))
    {
        throw new AppException('Last Name is required', 2);
    }
    }
}

try {
    new Students('', '');
} catch (Exception $e) {
    echo $e;
}