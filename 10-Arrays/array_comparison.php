<?php

$a = array('one'=>'php', 'two'=>'javascript', 'three'=>'python');
$b = array('python', 'javascript', 'php');

if(array_values(array_reverse($a)) === $b) {
    echo 'true';
} else {
    echo 'false';
}

echo '<br />';

if(array_values($a) === $b) {
    echo 'true';
} else {
    echo 'false';
}

echo '<br />';

if(array_reverse($a) === $b) {
    echo 'true';
} else {
    echo 'false';
}