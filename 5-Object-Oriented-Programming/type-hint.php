<?php

function myErrorHandler2TypeHint($errno, $errstr, $errfile, $errline, $errcontext)
{
    var_dump($errno, $errstr, $errfile, $errline, $errcontext);
}

function somefunc( DOMDocument $param )
{
  if($param instanceof DOMDocument)
    return 0;
  else
    return 1;
}

set_error_handler("myErrorHandler2TypeHint");

echo somefunc('abcd');

restore_error_handler();